#ifndef AD_READ_SM
#define AD_READ_SM
// ADMulti.h
// Setup up ADC0 to convert up to 4 channels using SS2

#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"

typedef enum { InitPState } ADReadState_t ;

bool InitADReadSM ( uint8_t Priority );
bool PostADReadSM( ES_Event_t ThisEvent );
ES_Event_t RunADReadSM( ES_Event_t ThisEvent );

#endif
