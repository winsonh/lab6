/****************************************************************************
 
  Header file for Test Harness Service0 
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef Interrupt_H
#define Interrupt_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"    


void InitInputCapturePeriod(void);
// void PeriodicIntResponse(void);
uint32_t GetPeriod(void);

#endif /* Interrupt_H */
