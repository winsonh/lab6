/****************************************************************************
 
  Header file for Test Harness Service0 
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef PWMLib_H
#define PWMLib_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"    


void InitPWM(void);
void Set100_0DC(void);
void RestoreDC(void);
void SetDutyCycle(uint8_t DutyCycle);

#endif /* PWMLib_H */

